/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class HtblFailureInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "htbl_failure_information_id_seq")
    @SequenceGenerator(name = "htbl_failure_information_id_seq", sequenceName = "htbl_failure_information_id_seq", allocationSize = 1)
    @Column(name = "hid", updatable = false)
    private Long hid;
    private Long haction;
    private Date hdate;
    private String huser;
    private Long id;
    private UUID uuid;
    private Long versionNumber;
    private String title;
    private String description;
    private String responsibility;
    private String voltageLevel;
    private String pressureLevel;
    private Date failureBegin;
    private Date failureEndPlanned;
    private Date failureEndResupplied;
    private String internalRemark;
    private String postcode;
    private String city;
    private String district;
    private String street;
    private String housenumber;
    private String stationId;
    private String stationDescription;
    private String stationCoords;
    private BigDecimal longitude;
    private BigDecimal latitude;
    @Column( name = "object_ref_ext_system")
    private String objectReferenceExternalSystem;
    private String publicationStatus;
    private String publicationFreetext;
    private Boolean condensed;
    private String addressType;
    private String faultLocationArea;

    @CreatedDate
    @Column(name = "create_date")
    private Date createDate;

    @CreatedBy
    @Column(name = "create_user")
    private String createUser;

    @LastModifiedDate
    @Column(name = "mod_date")
    private Date modDate;

    @LastModifiedBy
    @Column(name = "mod_user")
    private String modUser;

    @ManyToOne
    @JoinColumn( name = "fk_ref_failure_classification")
    private RefFailureClassification refFailureClassification;

    @ManyToOne
    @JoinColumn( name = "fk_ref_status_intern")
    private RefStatus refStatusIntern;

    @ManyToOne
    @JoinColumn( name = "fk_ref_branch")
    private RefBranch refBranch;

    @ManyToOne
    @JoinColumn( name = "fk_ref_radius")
    private RefRadius refRadius;

    @ManyToOne
    @JoinColumn( name = "fk_ref_expected_reason")
    private RefExpectedReason refExpectedReason;
}
