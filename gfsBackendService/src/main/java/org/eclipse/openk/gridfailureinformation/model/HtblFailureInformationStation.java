/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table( name = "HTBL_FAILINFO_STATION")
public class HtblFailureInformationStation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "HTBL_FAILINFO_STATION_ID_SEQ")
    @SequenceGenerator(name = "HTBL_FAILINFO_STATION_ID_SEQ", sequenceName = "HTBL_FAILINFO_STATION_ID_SEQ", allocationSize = 1)
    @Column(name = "hid", updatable = false)
    private Long hid;
    private Date hdate;

    private Long fkTblFailureInformation;
    private Long versionNumber;
    private String stationStationId;

}
