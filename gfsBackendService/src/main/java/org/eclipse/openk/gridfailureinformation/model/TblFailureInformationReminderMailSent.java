/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table( name = "tbl_failinfo_remind_mail_sent" )
public class TblFailureInformationReminderMailSent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_failinfo_reminsent_id_seq")
    @SequenceGenerator(name = "tbl_failinfo_reminsent_id_seq", sequenceName = "tbl_failinfo_reminsent_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "fk_tbl_failure_information", referencedColumnName = "id")
    private TblFailureInformation tblFailureInformation;

    private Boolean mailSent;
    private Date dateMailSent;

}
