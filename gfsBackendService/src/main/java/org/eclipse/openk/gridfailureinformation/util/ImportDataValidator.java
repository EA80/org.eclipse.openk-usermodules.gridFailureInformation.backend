/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.viewmodel.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ImportDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

@Component
@Log4j2
public class ImportDataValidator {

    @Autowired
    ObjectMapper objectMapper;

    public ForeignFailureDataDto readSafeForeignFailureInfo(ImportDataDto importDataDto) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> violationsImportData = validator.validate(importDataDto);

        if (!violationsImportData.isEmpty()) {
            String msgHeader = "Import Data Validation failed!";
            logImportViolations(violationsImportData, msgHeader);
            return null;
        }

        ForeignFailureDataDto foreignFailureDataDto;
        try {
            foreignFailureDataDto = objectMapper.readValue(importDataDto.getMessageContent(), ForeignFailureDataDto.class);
        } catch (JsonProcessingException e) {
            log.warn("Error importing JSON-Message", e);
            return null;
        }
        Set<ConstraintViolation<Object>> violationsForeignFailure = validator.validate(foreignFailureDataDto);

        if (!violationsForeignFailure.isEmpty()) {
            logImportViolations(violationsForeignFailure, "Foreign Failure Validation failed!");
            return null;
        }
        return foreignFailureDataDto;
    }

    private void logImportViolations(Set<ConstraintViolation<Object>> violationsImportData, String msgHeader) {
        for (ConstraintViolation violation : violationsImportData) {
            String  msg = msgHeader + " " + violation.getPropertyPath()
                    + " [" + violation.getInvalidValue() + "] : "
                    + violation.getMessageTemplate() + " "
                    + violation.getMessage();
            log.warn(msg);
        }
    }

}
