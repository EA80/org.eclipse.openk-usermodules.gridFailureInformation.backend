/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.DistributionTextPlaceholderService;
import org.eclipse.openk.gridfailureinformation.util.ResourceLoaderBase;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionTextPlaceholderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Log4j2
@Configuration
public class ResourceConfig {

    private static final String EMAIL_BTN_TEMPLATE_PATH = "email/emailTemplateBtn.html";

    @Value("${spring.settings.isUseHtmlEmailBtnTemplate:true}")
    private boolean isUseHtmlEmailBtnTemplate;

    @Autowired
    DistributionTextPlaceholderService placeholderService;

    private String htmlEmailBtnTemplate;

    @Bean
    public DistributionTextPlaceholderDto placeholderLoader() {
        return placeholderService.getPlaceholder();
    }

    @PostConstruct
    private void init(){
        if (isUseHtmlEmailBtnTemplate) {
            htmlEmailBtnTemplate = new ResourceLoaderBase().loadStringFromResource(EMAIL_BTN_TEMPLATE_PATH);
        }
    }

    public String getHtmlEmailBtnTemplate() {
        return htmlEmailBtnTemplate;
    }
}
