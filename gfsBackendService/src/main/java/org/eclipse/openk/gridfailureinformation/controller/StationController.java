/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.StationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/stations")
public class StationController {

    @Autowired
    private StationService stationService;

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeige aller Stationen")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<StationDto> findStations() {
        return stationService.getStations();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Holen der Polygon-Koordinaten für die  übergebenen Stationen")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @PostMapping("/polygon-coordinates")
    public List<ArrayList<BigDecimal>> getPolygonCoordinates(
            @ApiParam(name="listStationUuids", value= "Liste der Stationen, für die gemeinsame Polygon-Koordinaten erstellt werden sollen", required = true)
            @RequestBody List<UUID> listStationUuids) {
        return stationService.getPolygonCoordinates(listStationUuids);
    }
}
