/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper;

import org.eclipse.openk.gridfailureinformation.mapper.tools.AddressSplitterMerger;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HistFailureInformationMapper {
    @Mappings({
            @Mapping(source = "refFailureClassification.uuid", target = "failureClassificationId"),
            @Mapping(source = "refFailureClassification.classification", target = "failureClassification"),
            @Mapping(source = "refStatusIntern.uuid", target = "statusInternId"),
            @Mapping(source = "refStatusIntern.status", target = "statusIntern"),
            @Mapping(source = "refBranch.uuid", target = "branchId"),
            @Mapping(source = "refBranch.name", target = "branch"),
            @Mapping(source = "refBranch.colorCode", target = "branchColorCode"),
            @Mapping(source = "refRadius.uuid", target = "radiusId"),
            @Mapping(source = "refRadius.radius", target = "radius"),
            @Mapping(source = "refExpectedReason.uuid", target = "expectedReasonId"),
            @Mapping(source = "refExpectedReason.text", target = "expectedReasonText")
    })
    FailureInformationDto toFailureInformationDto(HtblFailureInformation htblFailureInformation);

    @Mappings({
            @Mapping(target = "refFailureClassification.uuid", source = "failureClassificationId"),
            @Mapping(target = "refFailureClassification.classification", source = "failureClassification"),
            @Mapping(target = "refStatusIntern.uuid", source = "statusInternId"),
            @Mapping(target = "refStatusIntern.status", source = "statusIntern"),
            @Mapping(target = "refBranch.uuid", source = "branchId"),
            @Mapping(target = "refBranch.name", source = "branch"),
            @Mapping(target = "refBranch.colorCode", source = "branchColorCode"),
            @Mapping(target = "refRadius.uuid", source = "radiusId"),
            @Mapping(target = "refRadius.radius", source = "radius"),
            @Mapping(target = "refExpectedReason.uuid", source = "expectedReasonId"),
            @Mapping(target = "refExpectedReason.text", source = "expectedReasonText")
    })
    HtblFailureInformation toHtblFailureInformation(FailureInformationDto failureInformationDto);


    @AfterMapping
    default void splitFreetextAddress(HtblFailureInformation source, @MappingTarget FailureInformationDto failureInformationDto){
        AddressSplitterMerger.splitFreetextAddress(source.getAddressType(), source.getCity(),
                source.getDistrict(), source.getPostcode(), failureInformationDto);
    }

    @AfterMapping
    default void splitFreetextAddress(FailureInformationDto source, @MappingTarget HtblFailureInformation destTbl){
        AddressSplitterMerger.mergeFreetextAddress(source, destTbl);
    }
}
