/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

@Log4j2
@Data
public class GfiProcessSubject implements ProcessSubject {

    private FailureInformationDto failureInformationDto;
    private ProcessState stateInDb;
    private ProcessHelper processHelper;

    private GfiProcessSubject() {}


    private GfiProcessSubject(FailureInformationDto failureInformationDto, ProcessHelper processHelper ) {
        this.failureInformationDto = failureInformationDto;
        this.processHelper = processHelper;
    }

    public static GfiProcessSubject of(FailureInformationDto failureInformationDto, ProcessHelper processHelper ) {
        return new GfiProcessSubject(failureInformationDto, processHelper);

    }
}
