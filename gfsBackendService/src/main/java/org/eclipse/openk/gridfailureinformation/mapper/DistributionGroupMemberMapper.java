/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper;

import com.google.common.collect.Lists;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DistributionGroupMemberMapper {
    @Mappings({
            @Mapping(source = "tblDistributionGroup.uuid", target = "distributionGroupUuid"),
            @Mapping(source = "tblDistributionGroup.name", target = "distributionGroup")
    })
    DistributionGroupMemberDto toDistributionGroupMemberDto(TblDistributionGroupMember tblDistributionGroupMember);

    @AfterMapping
    default void mapPostcodesToList(TblDistributionGroupMember srcTbl, @MappingTarget DistributionGroupMemberDto targetDto) {
        List<String> result =
                Lists.newArrayList(
                    Optional.ofNullable(srcTbl.getPostcodes()).orElse("")
                    .split(","));

        targetDto.setPostcodeList(result.stream()
                 .filter( x -> !x.isEmpty() )
                 .sorted( String::compareTo )
                        .collect(Collectors.toList())
        );
    }


    @Mappings({
            @Mapping(source = "distributionGroupUuid", target = "tblDistributionGroup.uuid"),
            @Mapping(source = "distributionGroup", target = "tblDistributionGroup.name")
    })
    TblDistributionGroupMember toTblDistributionGroupMember(DistributionGroupMemberDto distributionGroupMemberDto);

    @AfterMapping
    default void mapPostcodesFromList(DistributionGroupMemberDto srcDto, @MappingTarget TblDistributionGroupMember targetTbl) {
        StringBuilder sb = new StringBuilder();
        Optional.ofNullable(srcDto.getPostcodeList()).orElse( new ArrayList<>()).stream()
                .filter( x -> x!=null && !x.isEmpty() )
                .sorted(String::compareTo)
                .forEach(x -> sb.append("," + x));
        targetTbl.setPostcodes(
                sb.toString().replaceFirst(",", "" ).trim()
        );
    }



}
