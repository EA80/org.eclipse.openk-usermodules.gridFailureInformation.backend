/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationDistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/grid-failure-informations")
public class FailureInformationDistributionGroupController {

    @Autowired
    private FailureInformationDistributionGroupService failureInformationDistributionGroupService;

    @Autowired
    private FailureInformationService failureInformationService;

    @Autowired
    private DistributionGroupService distributionGroupService;


    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @GetMapping("/{failureInfoUuid}/distribution-groups")
    @ApiOperation(value = "Anzeigen alle Verteilergruppen zu einer Störungsinformation")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Verteilergruppe konnt nicht zugeordnet werden")})
    @ResponseStatus(HttpStatus.OK)
    public List<DistributionGroupDto> getDistributionGroupsForFailureInformation(@PathVariable UUID failureInfoUuid) {
        return failureInformationDistributionGroupService.findDistributionGroupsByFailureInfo(failureInfoUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @PostMapping("/{failureInfoUuid}/distribution-groups")
    @ApiOperation(value = "Zuordnen einer Verteilergruppe zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Verteilergruppe erfolgreich zugeordnet"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<FailureInformationDistributionGroupDto> insertFailureInfoDistributionGroup(
            @PathVariable UUID failureInfoUuid,
            @Validated @RequestBody DistributionGroupDto dgDto) {

        FailureInformationDistributionGroupDto savedGfDgDto = failureInformationDistributionGroupService.insertFailureInfoDistributionGroup(failureInfoUuid, dgDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{groupid}")
                .buildAndExpand(savedGfDgDto.getDistributionGroupId())
                .toUri();
        return ResponseEntity.created(location).body(savedGfDgDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @DeleteMapping("/{failureInfoUuid}/distribution-groups/{groupUuid}")
    @ApiOperation(value = "Löschen der Zuordnung einer Verteilergruppe zu einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Verteilergruppenzuordnung erfolgreich gelöscht"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden")
    })
    public void deleteFailureInfoDistributionGroup(
            @PathVariable UUID failureInfoUuid,
            @PathVariable UUID groupUuid) {
        failureInformationDistributionGroupService.deleteFailureInfoDistributionGroup(failureInfoUuid, groupUuid);
    }

}
