/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.service.AddressService;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/addresses")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeigen aller Addressen nach Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<AddressDto> getAddresses(
            @RequestParam("branch") Optional<String> branch) {
        return addressService.getAddresses(branch);
    }

    @GetMapping("/{uuid}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeigen einer bestimmten Adresse")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Adresse wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public AddressDto readAddress(
            @PathVariable UUID uuid) {
        return addressService.getAdressByUuid(uuid);
    }

    @ApiOperation(value = "Anzeige aller PLZs einer Sparte oder gefiltert nach Ort und Ortsteil und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/postcodes")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findPostcodes(
            @RequestParam("community") Optional<String> community,
            @RequestParam("district") Optional<String> district,
            @RequestParam("branch") Optional<String> branch
    ) {
        if( community.isPresent() && district.isPresent()) {
            return addressService.getPostcodes(community.get(), district.get(), branch);
        }
        else if( community.isPresent() || district.isPresent() ) {
            throw new BadRequestException("query.param.combination.not.allowed");
        }
        else {
            return addressService.getPostcodes(branch);
        }
    }
    @ApiOperation(value = "Anzeige aller Orte nach Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/communities")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findAllCommunities(@RequestParam("branch") Optional<String> branch) {
        return addressService.getCommunities(branch);
    }


    @ApiOperation(value = "Anzeige aller Ortsteile nach Ort und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/districts/{community}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findAllDistrictsByCommunity(
            @PathVariable String community,
            @RequestParam("branch") Optional<String> branch) {
        return addressService.getDistricts(community, branch);
    }

    @ApiOperation(value = "Anzeige aller Orte nach Postleitzahl und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/communities/{postcode}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findAllCommunitiesByPostcode(
            @PathVariable String postcode,
            @RequestParam("branch") Optional<String> branch) {
        return addressService.getCommunities(postcode, branch);
    }

    @ApiOperation(value = "Anzeige Ortsteile nach Postleitzahl und Ort und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/districts")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findDistricts(
            @RequestParam("postcode") String postcode,
            @RequestParam("community") String community,
            @RequestParam("branch") Optional<String> branch
    ) {
        return addressService.getDistricts(postcode, community, branch);
    }

    @ApiOperation(value = "Anzeige Strassen nach Postleitzahl und Ort und Ortsteil (optional) und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/streets")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<String> findStreets(
            @RequestParam("postcode") String postcode,
            @RequestParam("community") String community,
            @RequestParam("district") Optional<String> district,
            @RequestParam("branch") Optional<String> branch
    ) {
        return addressService.getStreets(postcode, community, district, branch);
    }

    @ApiOperation(value = "Anzeige Hausnummern nach Postleitzahl und Ort und Strasse und Sparte (optional)")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping("/housenumbers")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<HousenumberUuidDto> findHousenumbers(
            @RequestParam("postcode") String postcode,
            @RequestParam("community") String community,
            @RequestParam("street") String street,
            @RequestParam("branch") Optional<String> branch
    ) {
        return addressService.getHousenumbers(postcode, community, street, branch);
    }

}
