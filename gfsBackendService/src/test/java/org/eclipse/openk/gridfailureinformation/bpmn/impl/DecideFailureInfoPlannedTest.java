/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoPlanned;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.NO;
import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.YES;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
class DecideFailureInfoPlannedTest {
    @Qualifier("myFailureInformationService")
    @Autowired
    private FailureInformationService failureInformationService;

    @MockBean
    private ProcessHelper processHelper;
    @MockBean
    private StatusRepository statusRepository;
    @MockBean
    private FailureInformationRepository failureInformationRepository;
    @MockBean
    private StationRepository stationRepository;
    @MockBean
    private AddressRepository addressRepository;

    @Test
    void shouldcall_DecideFailureInfoPlanned_Result_Created() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        refStatus.setId(GfiProcessState.NEW.getStatusValue());

        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        fiDto.setStatusInternId(UUID.randomUUID());

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        when(processHelper.isFailureInfoPlanned(any( FailureInformationDto.class ))).thenReturn(false);
        when(statusRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(failureInformationRepository.save(any(TblFailureInformation.class))).thenReturn(fiTbl);
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);

        FailureInformationDto savedDto = failureInformationService.updateFailureInfo(fiDto);

        verify(processHelper, times(1))
                .storeFailureFromViewModel(any( FailureInformationDto.class ));
    }

    @Test
    void shouldDecideYes() throws ProcessException {
        when( processHelper.isFailureInfoPlanned(any()))
                .thenReturn(true);
        GfiProcessSubject subject = GfiProcessSubject.of(null, processHelper);
        DecideFailureInfoPlanned task = new DecideFailureInfoPlanned();

        assertEquals( YES, task.decide(subject) );
    }


    @Test
    void shouldDecideNo() throws ProcessException {
        when( processHelper.isFailureInfoPlanned(any()))
                .thenReturn(false);
        GfiProcessSubject subject = GfiProcessSubject.of(null, processHelper);
        DecideFailureInfoPlanned task = new DecideFailureInfoPlanned();

        assertEquals( NO, task.decide(subject) );
    }
}
