/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.model.Version;
import org.eclipse.openk.gridfailureinformation.repository.VersionRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.VersionDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class VersionServiceTest {
    @Qualifier("myVersionService")
    @Autowired
    private VersionService versionService;

    @MockBean
    private VersionRepository versionRepository;

    @Test
    public void shouldGetVersionProperly() {
        Version mockVersion = MockDataHelper.mockVersion();
        when(versionRepository.findById(any(Long.class))).thenReturn(Optional.of(mockVersion));
        VersionDto versionDto = versionService.getVersion();

        assertEquals( mockVersion.getVersion(), versionDto.getDbVersion() );
    }

    @Test
    public void shouldGetVersionCorrectlyInErrorCase() {
        when(versionRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        VersionDto versionDto = versionService.getVersion();
        assertEquals(Constants.DB_VERSION_NOT_PRESENT, versionDto.getDbVersion() );
    }

}
