/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.StationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc()
@ActiveProfiles("test")
public class StationControllerTest {

    @MockBean
    private StationService stationService;

    @MockBean
    private FailureInformationService failureInformationService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void shouldReturnStations() throws Exception {
        List<StationDto> stationDtoList = MockDataHelper.mockStationDtoList();
        when(stationService.getStations()).thenReturn(stationDtoList);

        mockMvc.perform(get("/stations"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnPolygonCoordinates() throws Exception {
        List<ArrayList<BigDecimal>> coordinatesList = MockDataHelper.mockPolygonCoordinatesList();
        when(failureInformationService.getPolygonCoordinatesForStationUuids(anyList())).thenReturn(coordinatesList);

        List<UUID> uuidList = MockDataHelper.mockUuidList();

        mockMvc.perform(post("/stations/polygon-coordinates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(uuidList)))
                .andExpect(status().is2xxSuccessful());
    }

}