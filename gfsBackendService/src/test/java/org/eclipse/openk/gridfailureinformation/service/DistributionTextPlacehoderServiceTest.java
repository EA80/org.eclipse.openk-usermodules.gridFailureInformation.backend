/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionTextPlaceholderDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
public class DistributionTextPlacehoderServiceTest {
    @Qualifier("myDistributionTextPlaceholderService")
    @Autowired
    private DistributionTextPlaceholderService placeholderService;

    @Test
    public void shouldGetPlaceholdersProperly() {

        DistributionTextPlaceholderDto placeholderDto = placeholderService.getPlaceholder();

        assertEquals("$Klassifikation$", placeholderDto.getFailureClassification());
        assertEquals("$Zuständigkeit$", placeholderDto.getResponsibility());
        assertEquals("$Spannungsebene$", placeholderDto.getVoltageLevel());
        assertEquals("$Störungsbeginn_gemeldet$", placeholderDto.getFailureBegin());
        assertEquals("$Voraussichtlicher_Grund$", placeholderDto.getExpectedReason());
        assertEquals("$Status_extern$", placeholderDto.getStatusExtern());
    }

}
