/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.service.AddressService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc()
@ActiveProfiles("test")
class AddressControllerTest {

    @MockBean
    private AddressService addressService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnAllAddresses() throws Exception {
        Optional<String> branchOptional = Optional.empty();
        List<AddressDto> dtos = MockDataHelper.mockAddressDtoList();
        when(addressService.getAddresses(any())).thenReturn(dtos);

        mockMvc.perform(get("/addresses?brach=S"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getAddresses(branchOptional);
    }

    @Test
    void shouldReturnASingleAddress() throws Exception {
        UUID uuid = UUID.randomUUID();
        Optional<String> branchOptional = Optional.empty();
        AddressDto dto = MockDataHelper.mockAddressDtoList().get(0);
        when(addressService.getAdressByUuid(eq(uuid))).thenReturn(dto);

        mockMvc.perform(get("/addresses/"+uuid.toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("community", is(  dto.getCommunity())));

        verify(addressService, times(1)).getAdressByUuid(eq(uuid));
    }


    @Test
    void shouldReturnPostCodes() throws Exception {
        Optional<String> branchOptional = Optional.empty();
        List<String> postcodes = MockDataHelper.mockPostCodes();
        when(addressService.getPostcodes(branchOptional)).thenReturn(postcodes);

        mockMvc.perform(get("/addresses/postcodes"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getPostcodes(branchOptional);
    }


    @Test
    void shouldReturnPostCodesWithException() throws Exception {
        mockMvc.perform(get("/addresses/postcodes?district=x"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnPostCodesForDistrictAndCommunity() throws Exception {
        Optional<String> branchOptional = Optional.empty();
        List<String> postcodes = MockDataHelper.mockPostCodes();
        when(addressService.getPostcodes(eq("y"), eq("x"), eq(branchOptional))).thenReturn(postcodes);

        mockMvc.perform(get("/addresses/postcodes?district=x&community=y"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getPostcodes(eq("y"), eq("x"), eq(branchOptional));
    }


    @Test
    void shouldReturnPostCodesForBranch() throws Exception {
        String branch = "B";
        Optional<String> branchOptional = Optional.of(branch);
        List<String> postcodes = MockDataHelper.mockPostCodes();
        when(addressService.getPostcodes(branchOptional)).thenReturn(postcodes);

        mockMvc.perform(get("/addresses/postcodes?branch=" + branch))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getPostcodes(branchOptional);
    }

    @Test
    void shouldReturnCommunitiesForBranch() throws Exception {
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getCommunities(branchOptional)).thenReturn(strings);

        mockMvc.perform(get("/addresses/communities/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getCommunities(branchOptional);
    }

    @Test
    void shouldReturnCommunities() throws Exception {
        String postcode = "71111";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getCommunities(postcode, branchOptional)).thenReturn(strings);

        mockMvc.perform(get("/addresses/communities/" + postcode))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getCommunities(postcode, branchOptional);
    }

    @Test
    void shouldReturnCommunitiesForPostcodeAndBranch() throws Exception {
        String branch = "B";
        String postcode = "71111";
        Optional<String> branchOptional = Optional.of(branch);
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getCommunities(postcode, branchOptional)).thenReturn(strings);

        mockMvc.perform(get("/addresses/communities/" + postcode + "?branch=" + branch))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getCommunities(postcode, branchOptional);
    }

    @Test
    void shouldReturnDistricts() throws Exception {
        String postcode = "71111";
        String community = "com1";
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getDistricts(postcode, community, branchOptional)).thenReturn(strings);

        mockMvc.perform(
                get("/addresses/districts")
                        .param("postcode", postcode)
                        .param("community", community)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getDistricts(postcode, community, branchOptional);
    }

    @Test
    void shouldReturnDistrictsForBranch() throws Exception {
        String postcode = "71111";
        String community = "com1";
        String branch = "B";
        Optional<String> branchOptional = Optional.of(branch);
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getDistricts(postcode, community, branchOptional)).thenReturn(strings);

        mockMvc.perform(
                get("/addresses/districts?branch=" + branch)
                        .param("postcode", postcode)
                        .param("community", community)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getDistricts(postcode, community, branchOptional);
    }

    @Test
    void shouldReturnDistrictsForCommunityOnly() throws Exception {
        String community = "com1";
        String branch = "B";
        Optional<String> branchOptional = Optional.of(branch);
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getDistricts(eq(community), eq(branchOptional))).thenReturn(strings);

        mockMvc.perform(
                get("/addresses/districts/"+community+"?branch=" + branch)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getDistricts(eq(community), eq(branchOptional));
    }


    @Test
    void shouldReturnStreets() throws Exception {
        String postcode = "71111";
        String community = "com1";
        Optional<String> district = Optional.of("district1");
        Optional<String> branchOptional = Optional.empty();
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getStreets(postcode, community, district, branchOptional)).thenReturn(strings);

        mockMvc.perform(
                    get("/addresses/streets")
                    .param("postcode", postcode)
                    .param("community", community)
                    .param("district", "district1")
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getStreets(postcode, community, district, branchOptional);
    }

    @Test
    void shouldReturnStreetsForBranch() throws Exception {
        String postcode = "71111";
        String community = "com1";
        String branch = "B";
        Optional<String> district = Optional.of("district1");
        Optional<String> branchOptional = Optional.of(branch);
        List<String> strings = MockDataHelper.mockStringList();
        when(addressService.getStreets(postcode, community, district, branchOptional)).thenReturn(strings);

        mockMvc.perform(
                get("/addresses/streets?branch=" + branch)
                        .param("postcode", postcode)
                        .param("community", community)
                        .param("district", "district1")
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getStreets(postcode, community, district, branchOptional);
    }

    @Test
    void shouldReturnHousenumbers() throws Exception {
        String postcode = "71111";
        String community = "com1";
        String street = "street1";
        Optional<String> branchOptional = Optional.empty();
        List<HousenumberUuidDto> housenumberUuidDtos = MockDataHelper.mockHousnumberUuidList();
        when(addressService.getHousenumbers(postcode, community, street, branchOptional)).thenReturn(housenumberUuidDtos);

        mockMvc.perform(
                get("/addresses/housenumbers")
                        .param("postcode", postcode)
                        .param("community", community)
                        .param("street", street)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getHousenumbers(postcode, community, street, branchOptional);
    }

    @Test
    void shouldReturnHousenumbersForBranch() throws Exception {
        String postcode = "71111";
        String community = "com1";
        String street = "street1";
        String branch = "B";
        Optional<String> branchOptional = Optional.of(branch);
        List<HousenumberUuidDto> housenumberUuidDtos = MockDataHelper.mockHousnumberUuidList();
        when(addressService.getHousenumbers(postcode, community, street, branchOptional)).thenReturn(housenumberUuidDtos);

        mockMvc.perform(
                get("/addresses/housenumbers?branch=" + branch)
                        .param("postcode", postcode)
                        .param("community", community)
                        .param("street", street)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(addressService, times(1)).getHousenumbers(postcode, community, street, branchOptional);
    }
}