/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc()
@ActiveProfiles("test")
public class DistributionGroupControllerTest {

    @MockBean
    private DistributionGroupService distributionGroupService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDistributionGroups() throws Exception {
        List<DistributionGroupDto> distributionGroupDtoList = MockDataHelper.mockDistributionGroupDtoList();
        when(distributionGroupService.getDistributionGroups()).thenReturn(distributionGroupDtoList);

        mockMvc.perform(get("/distribution-groups"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnARequestedDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();

        when(distributionGroupService.getDistributionGroupByUuid(any(UUID.class))).thenReturn(groupDto);

        mockMvc.perform(get("/distribution-groups/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("name", is(  groupDto.getName())));
    }

    @Test
    public void shouldExceptionIfNotFoundDistributionGroup() throws Exception {
        when(distributionGroupService.getDistributionGroupByUuid(any(UUID.class))).thenThrow( new NotFoundException());

        mockMvc.perform(get("/distribution-groups/"+UUID.randomUUID().toString()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldInsertDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();

        when( distributionGroupService.insertDistributionGroup(any(DistributionGroupDto.class))).thenReturn(groupDto);

        mockMvc.perform(post("/distribution-groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(jsonPath("$.name", is(groupDto.getName())))
                .andExpect(jsonPath("$.distributionTextPublish", is(groupDto.getDistributionTextPublish())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldUpdateDistributionGroup() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();

        when( distributionGroupService.updateGroup(any(DistributionGroupDto.class))).thenReturn(groupDto);

        mockMvc.perform(put("/distribution-groups/{groupUuid}", groupDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public void shouldUpdateDistributionGroupDueToException() throws Exception {
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();

        when( distributionGroupService.updateGroup(any(DistributionGroupDto.class))).thenReturn(groupDto);

        mockMvc.perform(put("/distribution-groups/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(groupDto)))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void shouldDeleteDistributionGroup() throws Exception {
        mockMvc.perform(delete("/distribution-groups/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}