/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.importadresses.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InternalServerErrorExceptionTest {
    @Test
    public void testExceptionCreationForCoverage() {
        new InternalServerErrorException("testOnly");
        Exception innerException = new InternalServerErrorException("inner Exception");
        assertEquals( innerException.getMessage(), "inner Exception");
    }
}
