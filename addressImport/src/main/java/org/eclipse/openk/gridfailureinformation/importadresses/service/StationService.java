/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.importadresses.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.model.TblStation;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Log4j2
@Service
public class StationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private StationMapper stationMapper;

    public StationDto insertStation(StationDto stationDto){
        TblStation tblStationToSave = stationMapper.toTableStation(stationDto);
        tblStationToSave.setUuid(UUID.randomUUID());
        TblStation tblSavedStation = stationRepository.save(tblStationToSave);
        return stationMapper.toStationDto(tblSavedStation);
    }

    public StationDto updateOrInsertStationByG3efid(StationDto stationDto) {

        try {
            Optional<TblStation> optTblStation = stationRepository.findByStationId(stationDto.getStationId());

            if (optTblStation.isPresent() ) {
                TblStation tblStationToSave =  optTblStation.get();

                tblStationToSave.setSdox1(stationDto.getSdox1());
                tblStationToSave.setSdoy1(stationDto.getSdoy1());
                tblStationToSave.setG3efid(stationDto.getG3efid());
                tblStationToSave.setStationName(stationDto.getStationName());
                tblStationToSave.setLatitude(stationDto.getLatitude());
                tblStationToSave.setLongitude(stationDto.getLongitude());

                TblStation savedStation = stationRepository.save(tblStationToSave);
                return stationMapper.toStationDto(savedStation);
            } else {
                return insertStation(stationDto);
            }

        } catch (Exception ex) {

            log.warn("Adresse [sdo_x1= " + stationDto.getSdox1()
                    + ", sdo_y1= " + stationDto.getSdoy1()
                    + ", g3e_fid=" + stationDto.getG3efid()
                    + ", station_id=" + stationDto.getStationId()
                    + ", name=" + stationDto.getStationName()
                    + "]");

            return stationDto;
        }

    }
    @Transactional
    public  void deleteAllStations() {
        stationRepository.deleteAllInBatch();
    }

}
