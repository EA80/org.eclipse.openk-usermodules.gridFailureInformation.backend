package org.eclipse.openk.gridfailureinformation.sarisinterface.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Log4j2
public class WebServiceMessageSenderWithAuth extends HttpUrlConnectionMessageSender {


    private final String user;
    private final String password;

    public WebServiceMessageSenderWithAuth(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    protected void prepareConnection(HttpURLConnection connection)
            throws IOException {
        Base64.Encoder enc = Base64.getEncoder();
        String userpassword = user +":"+ password;
        String encodedAuthorization = enc.encodeToString(userpassword.getBytes(StandardCharsets.UTF_8));
        connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization); // NOSONAR Basic Authorization is used on the server side of this interface
        super.prepareConnection(connection);
    }
}
