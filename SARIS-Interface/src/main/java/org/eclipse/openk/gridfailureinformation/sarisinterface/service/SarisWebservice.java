
package org.eclipse.openk.gridfailureinformation.sarisinterface.service;


import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemein;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;

@Log4j2
public class SarisWebservice extends WebServiceGatewaySupport {

	@Value("${saris.apiUrl}")
	public String sarisApiUrl;

	@Value("${saris.user}")
	public String sarisUser;
	@Value("${saris.password}")
	public String sarisPassword;

	@Value("${saris.bisToleranz}")
	public int sarisBisToleranz;
	@Value("${saris.vonToleranz}")
	public int sarisVonToleranz;

	@Value("${saris.testIntegration.year}")
	public int testYearIntegration;
	@Value("${saris.testIntegration.month}")
	public int testMonthIntegration;
	@Value("${saris.testIntegration.day}")
	public int testDayIntegration;

	public GetAktuelleGVUsInfoAllgemeinResponse getAktuelleGVU(int sarisSparteId, boolean testCall) {
		log.info("getAktuelleGVU called");
		LocalDate localDate = LocalDate.now(ZoneId.of("Europe/Berlin"));
		if (testCall) localDate = LocalDate.of(testYearIntegration, testMonthIntegration, testDayIntegration);

		GregorianCalendar gcal = GregorianCalendar.from(localDate.atStartOfDay(ZoneId.of("Europe/Berlin")));
		XMLGregorianCalendar xcal = null;
		try {
			xcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
		} catch (DatatypeConfigurationException e) {
			log.error("Error while trying to convert to newXMLGregorianCalendar", e);
		}

		GetAktuelleGVUsInfoAllgemein request = new GetAktuelleGVUsInfoAllgemein();
		request.setBisToleranz(sarisBisToleranz);
		request.setVonToleranz(sarisVonToleranz);
		request.setSparteId(sarisSparteId);
		request.setDienstplanId(0);
		request.setDateTime(xcal);

		WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
		webServiceTemplate.setCheckConnectionForFault(true);
		webServiceTemplate.setCheckConnectionForError(true);
		webServiceTemplate.setMessageSender(new WebServiceMessageSenderWithAuth(sarisUser, sarisPassword));

		GetAktuelleGVUsInfoAllgemeinResponse response = (GetAktuelleGVUsInfoAllgemeinResponse) webServiceTemplate
				.marshalSendAndReceive(sarisApiUrl, request, new SoapActionCallback(
						"http://tempuri.org/GetAktuelleGVUsInfoAllgemein"));

		return response;
	}

}
