package org.eclipse.openk.gridfailureinformation.sarisinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SarisInterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SarisInterfaceApplication.class);
	}

}
