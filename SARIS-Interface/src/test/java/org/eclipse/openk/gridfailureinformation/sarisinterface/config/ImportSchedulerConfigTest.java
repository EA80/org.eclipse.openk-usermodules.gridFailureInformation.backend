package org.eclipse.openk.gridfailureinformation.sarisinterface.config;

import org.eclipse.openk.gridfailureinformation.sarisinterface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.ImportService;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.SarisWebservice;
import org.eclipse.openk.gridfailureinformation.sarisinterface.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportSchedulerConfigTest {

    @MockBean
    private SarisWebservice sarisWebservice;

    @SpyBean
    private ImportService importService;

    @Autowired
    private ImportSchedulerConfig importSchedulerConfig;

    @Test
    public void shoulImportUserNotification() {

        GetAktuelleGVUsInfoAllgemeinResponse mockedSarisResponse = MockDataHelper.getMockedSarisResponse(
                "sarisRealMockResponse.xml");

        when(sarisWebservice.getAktuelleGVU(anyInt(), anyBoolean())).thenReturn(mockedSarisResponse);

        importSchedulerConfig.scheduleTaskImportMessages();
        verify(importService, times(3)).importForeignFailures(anyInt());
    }
}
