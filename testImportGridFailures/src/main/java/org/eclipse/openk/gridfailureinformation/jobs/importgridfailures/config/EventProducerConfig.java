/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Configuration
@EnableIntegration
@Log4j2
public class EventProducerConfig {

    @Value("${spring.rabbitmq.failure_import_routingkey}")
    public String routingKey;
    @Value("${spring.rabbitmq.import_exchange}")
    public String exchangeName;
    @Value("${spring.rabbitmq.failure_import_queue}")
    public String queueName;



    @Autowired
    RabbitTemplate rabbitTemplate;

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public MessageChannel failureImportChannel() { return new DirectChannel(); }

    @Bean
    public Queue failureImportQueue() { return new Queue(queueName); }

    @Bean
    public Binding binding() { return BindingBuilder.bind(failureImportQueue()).to(exchange()).with(routingKey); }

    @Bean
    @ServiceActivator(inputChannel = "failureImportChannel")
    public MessageHandler messageHandler() {

        return message -> rabbitTemplate.convertAndSend(exchangeName, routingKey, message.getPayload(), message1 -> {
            message1.getMessageProperties().getHeaders().put("metaId", message.getHeaders().get("metaId"));
            message1.getMessageProperties().getHeaders().put("description", message.getHeaders().get("description"));
            message1.getMessageProperties().getHeaders().put("source", message.getHeaders().get("source"));
            return message1;
        });
    }
}
