/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.controller;

import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.TestImportGridFailuresApplication;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest(classes = TestImportGridFailuresApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ImportControllerTest {

    @MockBean
    private ImportService importService;

    @MockBean
    private ImportService messageImportChannel;

    @Autowired
    private MockMvc mockMvc;


//    @Test
//    public void shouldCallImport() throws Exception {
//        ForeignFailureDto foreignFailureDto = MockDataHelper.mockForeignFailureDto();
//
//        ImportService spy = Mockito.spy(messageImportChannel);
//        Mockito.doNothing().when(spy).setHeader(anyString(), anyString());
//
//        mockMvc.perform(post("/import"))
//                .andExpect(status().is2xxSuccessful());
//
//
//    }
}