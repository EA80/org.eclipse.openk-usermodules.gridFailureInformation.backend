/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.*;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.mapper.StoerungsauskunftMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "rabbitmq")
public class ImportExportService {

    @Autowired
    private StoerungsauskunftApi stoerungsauskunftApi;

    @Autowired
    private MessageChannel failureImportChannel;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    StoerungsauskunftMapper stoerungsauskunftMapper;

    @Value("${gridFailureInformation.autopublish:false}")
    private boolean autopublish;
    @Value("${gridFailureInformation.onceOnlyImport:false}")
    private boolean onceOnlyImport;
    @Value("${gridFailureInformation.excludeEquals:true}")
    private boolean excludeEquals;
    @Value("${gridFailureInformation.excludeAlreadyEdited:true}")
    private boolean excludeAlreadyEdited;

    @Value("${stoerungsauskunft.additiveMode:false}")
    private boolean additiveMode;

    public void importUserNotifications() {

        List<StoerungsauskunftUserNotification> userNotificationList =
                stoerungsauskunftApi.getUserNotification(1);

        for (StoerungsauskunftUserNotification userNotification : userNotificationList) {
            pushForeignFailure(createForeignFailureMessageDto(userNotification));
        }
    }

    private ForeignFailureMessageDto createForeignFailureMessageDto(StoerungsauskunftUserNotification userNotification) {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId(userNotification.getId());
        foreignFailureMessageDto.setSource(Constants.SRC_STOERUNGSAUSKUNFT_DE);
        ForeignFailureDataDto foreignFailureDataDto = createForeignFailureData(userNotification);
        foreignFailureMessageDto.setPayload(foreignFailureDataDto);
        return foreignFailureMessageDto;
    }

    private ForeignFailureDataDto createForeignFailureData(StoerungsauskunftUserNotification userNotification) {
        ForeignFailureDataDto foreignFailureDataDto = stoerungsauskunftMapper.toForeignFailureDataDto(userNotification);
        foreignFailureDataDto.setBranch(Constants.BRANCH_ELECTRICITY);
        foreignFailureDataDto.setPlanned(false);
        foreignFailureDataDto.setAutopublish(autopublish);
        foreignFailureDataDto.setOnceOnlyImport(onceOnlyImport);
        foreignFailureDataDto.setExcludeEquals(excludeEquals);
        foreignFailureDataDto.setExcludeAlreadyEdited(excludeAlreadyEdited);
        return foreignFailureDataDto;
    }

    public void pushForeignFailure(ForeignFailureMessageDto foreignFailureMessageDto) {

        try {
            failureImportChannel.send(
                    MessageBuilder.withPayload(
                            objectMapper.writeValueAsString(foreignFailureMessageDto.getPayload()))
                            .setHeader("metaId", foreignFailureMessageDto.getMetaId())
                            .setHeader("description", foreignFailureMessageDto.getDescription())
                            .setHeader("source", foreignFailureMessageDto.getSource())
                            .build());
            log.info("Succesfully imported message from stoerungsauskunft.de with metaId: " + foreignFailureMessageDto.getMetaId());
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.message");
        }

    }

    public void exportStoerungsauskunftOutage(RabbitMqMessageDto rabbitMqMessageDto) {

        FailureInformationDto failureInformationDto = rabbitMqMessageDto.getFailureInformationDto();
        StoerungsauskunftOutage stoerungsauskunftOutageExport = stoerungsauskunftMapper.toStoerungsauskunftOutage(failureInformationDto);
        stoerungsauskunftOutageExport.setId(failureInformationDto.getUuid().toString());

        setGeoType(failureInformationDto, stoerungsauskunftOutageExport);
        setPlanned(failureInformationDto, stoerungsauskunftOutageExport);

        List<StoerungsauskunftOutage> stoerungsauskunftOutageExportList = new ArrayList<>();
        stoerungsauskunftOutageExportList.add(stoerungsauskunftOutageExport);

        Response response = stoerungsauskunftApi.postOutage(stoerungsauskunftOutageExportList, additiveMode);
        if (response.status() >= 400) {
            log.error("Error while trying to post message to stoerungsauskunft.de with metaId: " + failureInformationDto.getUuid());
            throw new InternalServerErrorException("could.not.post.message.to.external.service");
        }
        log.info("Succesfully posted message to stoerungsauskunft.de with metaId: " + failureInformationDto.getUuid());
    }

    private void setPlanned(FailureInformationDto failureInformationDto, StoerungsauskunftOutage stoerungsauskunftOutageExport) {
        stoerungsauskunftOutageExport.setType(Constants.UNPLANNED_OUTAGE);
        if (failureInformationDto.isPlanned()) {
            stoerungsauskunftOutageExport.setType(Constants.PLANNED_OUTAGE);
        }
    }

    private void setGeoType(FailureInformationDto failureInformationDto, StoerungsauskunftOutage stoerungsauskunftOutageExport) {
        stoerungsauskunftOutageExport.setGeoType(1);
        if (failureInformationDto.getRadius() != null) {
            stoerungsauskunftOutageExport.setGeoType(2);
        }
        if (failureInformationDto.getAddressPolygonPoints() != null) {
            stoerungsauskunftOutageExport.setGeoType(3);
        }
    }

}
