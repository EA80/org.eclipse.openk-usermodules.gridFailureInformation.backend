/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.rabbitMq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.mapper.StoerungsauskunftMapperImpl;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@EntityScan(basePackageClasses = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration( initializers = {ConfigFileApplicationContextInitializer.class})
@TestPropertySource("spring.config.location=classpath:application.yml")
@EnableRabbit
public class TestConfiguration {

    @Bean
    ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public StoerungsauskunftMapperImpl stoerungsauskunftMapper() {return new StoerungsauskunftMapperImpl();}

    @Bean
    public MessageChannel failureImportChannel() {return mock(MessageChannel.class);}

    @Bean
    @Qualifier("rabbitMqConfig")
    public RabbitMqConfig rabbitMqConfig() {
        RabbitMqConfig rabbitMqConfigMock = mock(RabbitMqConfig.class);
        doNothing().when(rabbitMqConfigMock).buildAllQueues();
        return rabbitMqConfigMock;
    }

    @Bean
    public TestRabbitTemplate template() throws IOException {
        return new TestRabbitTemplate(connectionFactory());
    }

    @Bean
    public ConnectionFactory connectionFactory() throws IOException {
        ConnectionFactory factory = mock(ConnectionFactory.class);
        Connection connection = mock(Connection.class);
        Channel channel = mock(Channel.class);
        willReturn(connection).given(factory).createConnection();
        willReturn(channel).given(connection).createChannel(anyBoolean());
        given(channel.isOpen()).willReturn(true);
        return factory;
    }

}
